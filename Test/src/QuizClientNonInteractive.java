

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import server.AlternativeImpl;
import server.CompleteQuestionImpl;
import Quiz.Alternative;
import Quiz.CompleteQuestion;
import Quiz.Question;
import Quiz.QuizServer;
import Quiz.QuizServerHelper;

/**
 * This is the client side Non-interactive Script that initializes the ORB 
 * including the name server references.
 * Additionally, it pushes 10 Questions to the server when it's started.
 */
public class QuizClientNonInteractive {

	/**
	 * The main method is the only existing method in this class and executes what is decribed in the class description.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try{
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			// get the root naming context
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			
			// Use NamingContextExt instead of NamingContext. This is 
			// part of the Interoperable naming Service.  
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			// resolve the Object Reference in Naming
			String name = "Quiz";
			
			//narrow-methode is only for adapting the datatype to the object-type, perhaps we don't need it
			QuizServer quizImpl = QuizServerHelper.narrow(ncRef.resolve_str(name));
			
			// question 1
			CompleteQuestion cQ = new CompleteQuestionImpl();
			cQ.id = 1;
			cQ.sentence = "What is not a system model for distributed system?";
			cQ.alternatives = new Alternative[3];
			
			Alternative a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "Physical model";
			
			Alternative b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "Logical model";
			
			Alternative c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "Fundamental model";
		
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'b';
			
			quizImpl.newQuestion(cQ);
			
			//question 2
			cQ = new CompleteQuestionImpl();
			cQ.id=2;
			cQ.sentence ="Which characteristic is wrong for raw socket programming?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "Time decoupling";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "Transient";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "Mainly used for building higher-level abstractions";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'a';
			
			quizImpl.newQuestion(cQ);
			
			//question 3
			cQ = new CompleteQuestionImpl();
			cQ.id=3;
			cQ.sentence ="When should the push approch be used for Epidemic Dissemination?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "when many nodes are effected";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "when few nodes are infected";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "when no node is infected at all";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'b';
			
			quizImpl.newQuestion(cQ);
			
			//question 4
			cQ = new CompleteQuestionImpl();
			cQ.id=4;
			cQ.sentence ="What is not an implication of distributed systems?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "unreliable communication";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "independent failure of components";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "no concurrency";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'c';
			
			quizImpl.newQuestion(cQ);
			
			//question 5
			cQ = new CompleteQuestionImpl();
			cQ.id=5;
			cQ.sentence ="Which type of transperancy hides that a resource may be moved to another location while being in use?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "migration transparency";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "location transparency";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "relocation transparency";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'c';
			quizImpl.newQuestion(cQ);

			
			//question 6
			cQ = new CompleteQuestionImpl();
			cQ.id=6;
			cQ.sentence ="Which one is the right layering of middleware?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "applications, middleware services, RMI and RCP, request-response-protocolse + marshalling and external data representation, UDP and TCP";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "applications, middleware services, request-response-protocolse + marshalling and external data representation, RMI and RCP, UDP and TCP";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "applications, middleware services, RMI and RCP, UDP and TCP,request-response-protocolse + marshalling and external data representation";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'a';
			quizImpl.newQuestion(cQ);
			
		
			//question7
			cQ = new CompleteQuestionImpl();
			cQ.id=7;
			cQ.sentence ="Which statement is right for RMI interface processing?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "a server has 0 oder 1 dispatcher for each class representing a remote object";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "a server has no dispatchers for each class";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "a server has exactly 1 dispatcher for each class";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'c';
			quizImpl.newQuestion(cQ);
			
			//question 8
			cQ = new CompleteQuestionImpl();
			cQ.id=8;
			cQ.sentence ="Which statement is wrong for the CORBA IDL?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "it is computationally incomplete";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "it is syntactically oriented towards Java";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "it is not dependent on a specific programming language";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'b';
			quizImpl.newQuestion(cQ);
			
			//question 9
			cQ = new CompleteQuestionImpl();
			cQ.id=9;
			cQ.sentence ="What is a task of the object request broker (ORB)?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "it uses a special middleware to maintain RPC ";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "it invokes the client";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "it processes requests that arrive over a set of  connected sockets";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'c';
			quizImpl.newQuestion(cQ);

			//question 10
			cQ = new CompleteQuestionImpl();
			cQ.id=10;
			cQ.sentence ="What does marshalling mean?";
			cQ.alternatives = new Alternative[3];
			
			a = new AlternativeImpl();
			a.id = 'a';
			a.sentence = "transforming the memory representation of an object to a data format suitable for storage or transmission";
					
			b = new AlternativeImpl();
			b.id = 'b';
			b.sentence = "providing an endpoint of an inter-process communication flow across a computer network";
					
			c = new AlternativeImpl();
			c.id = 'c';
			c.sentence = "outlining a class that contains a description of the class' roles";
					
			cQ.alternatives[0] = a;
			cQ.alternatives[1] = b;		
			cQ.alternatives[2] = c;
			
			cQ.correctAlternatives = new char[1];
			cQ.correctAlternatives[0] = 'a';
			quizImpl.newQuestion(cQ);
			
			// Prints out one random question
			Question random = quizImpl.getRandomQuestion();
			
			System.out.println(random.sentence);
			for (int i = 0; i < random.alternatives.length; i++) {
				System.out.println(random.alternatives[i].id + ") " + random.alternatives[i].sentence);
			}			
		} catch (Exception e) {
			System.err.println("ERROR: " + e);
			e.printStackTrace(System.out);
		}

	}
}
