package HelloApp;


/**
* HelloApp/HelloOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Hello.idl
* Monday, September 17, 2012 2:25:42 PM CEST
*/

public interface HelloOperations 
{
  String sayHello (String message);
} // interface HelloOperations
