package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import server.AlternativeImpl;
import server.CompleteQuestionImpl;
import Quiz.Alternative;
import Quiz.CompleteQuestion;
import Quiz.Question;
import Quiz.QuizServer;
import Quiz.QuizServerHelper;
import Quiz.QuizServerPackage.QuizException;
import Quiz.QuizServerPackage.alternativesIdsHolder;

/**
 * This class represents the Interactive client.
 * 
 * To run this class, just configure the arguments this Java Application to:
 * 
 * -ORBInitialPort port -ORBInitialHost host
 * 
 * port defines the port of the server (name service)
 * host defines the host of the server (name service)
 *
 */
public class QuizClientInteractive {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			// get the root naming context
			org.omg.CORBA.Object objRef = orb
					.resolve_initial_references("NameService");

			// Use NamingContextExt instead of NamingContext. This is
			// part of the Interoperable naming Service.
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			// resolve the Object Reference in Naming
			String name = "Quiz";

			// narrow-methode is only for adapting the datatype to the
			// object-type, perhaps we don't need it
			QuizServer quizImpl = QuizServerHelper.narrow(ncRef
					.resolve_str(name));

			// GUI frame = new GUI(quizImpl);

			QuizClientInteractive.clientRoutine(quizImpl);

		} catch (Exception e) {
			// JDialog error = new JDialog();

			System.err.println("ERROR: " + e);
			e.printStackTrace(System.out);
		}
	}

	
	/**
	 * Prints just the variable output to stdout.
	 * 
	 * @param output
	 */
	private static void printCLI(String output) {
		System.out.println(output);
	}

	
	/**
	 * Represents the standard client routine.
	 * The object quizImpl references the server object.
	 * 
	 * 
	 * @param quizImpl
	 * @throws IOException
	 */
	private static void clientRoutine(QuizServer quizImpl) throws IOException {
		String menu = "___________________________\n1. Answer question\n2. Create new question\n3. Delete a question\n4. Get random question\n5. Quit\n";
		String currentLine;

		while (true) {
			InputStreamReader converter = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(converter);
			QuizClientInteractive.printCLI(menu);
			currentLine = in.readLine();

			if (currentLine.equals("1")) {
				int questionId;
				char answers[] = null;

				QuizClientInteractive
						.printCLI("Please enter valid a question ID");

				while (true) {
					try {
						currentLine = in.readLine();
						questionId = Integer.parseInt(currentLine);
					} catch (NumberFormatException e) {
						QuizClientInteractive
								.printCLI("Id not readable, please try again.");
						continue;
					}

					break;
				}

				boolean moreAnswers = true;

				while (moreAnswers) {
					QuizClientInteractive
							.printCLI("Specify the id of a correct answer.");
					char c;

					while (true) {
						try {
							currentLine = in.readLine();
							c = currentLine.charAt(0);
						} catch (IndexOutOfBoundsException e) {
							QuizClientInteractive
									.printCLI("Id of answer if not valid, please try again");
							continue;
						}
						break;
					}

					if (answers != null) {
						char temp[] = answers;
						answers = new char[temp.length + 1];
						for (int j = 0; j < temp.length; j++) {
							answers[j] = temp[j];
						}
						answers[temp.length] = c;
					} else {
						answers = new char[1];
						answers[0] = c;
					}

					QuizClientInteractive.printCLI("More answers? y or n");
					while (true) {
						currentLine = in.readLine();
						if (currentLine.equals("y"))
							moreAnswers = true;
						else if (currentLine.equals("n"))
							moreAnswers = false;
						else {
							QuizClientInteractive
									.printCLI("Can't read input, please try again");
							continue;
						}
						break;
					}
				}

				alternativesIdsHolder correct = new alternativesIdsHolder();
				boolean answerToQuestion = false;
				try {
					answerToQuestion = quizImpl.answerQuestion(questionId,
							answers, correct);
				} catch (QuizException e) {
					QuizClientInteractive.printCLI(e.reason);
					continue;
				}

				if (answerToQuestion)
					QuizClientInteractive
							.printCLI("Your answer to the question is right.");
				else {
					QuizClientInteractive
							.printCLI("Your answer to the question is wrong.");

					String tmp = "";
					for (int i = 0; i < correct.value.length; i++) {
						if (i < correct.value.length - 1)
							tmp += correct.value[i] + ", ";
						else
							tmp += correct.value[i];
					}
					QuizClientInteractive.printCLI("The right answers are: "
							+ tmp);

				}

			} else if (currentLine.equals("2")) {
				CompleteQuestion question = new CompleteQuestionImpl();

				QuizClientInteractive
						.printCLI("Please enter valid a question ID");

				while (true) {
					currentLine = in.readLine();

					try {
						question.id = Integer.parseInt(currentLine);
					} catch (NumberFormatException e) {
						QuizClientInteractive
								.printCLI("ID not valid, please try again.");
						continue;
					}

					QuizClientInteractive.printCLI("Please enter a question");
					currentLine = in.readLine();

					question.sentence = currentLine;

					QuizClientInteractive
							.printCLI("How many answers does your question have?");
					int amountAnswers;

					while (true) {
						try {
							currentLine = in.readLine();
							amountAnswers = Integer.parseInt(currentLine);
						} catch (NumberFormatException e) {
							QuizClientInteractive
									.printCLI("Amount of answers not readable, please try again.");
							continue;
						}

						if (amountAnswers <= 0) {
							QuizClientInteractive
									.printCLI("Amount of answers must be >= 0, please try again.");
							continue;
						}

						break;
					}

					question.alternatives = new Alternative[amountAnswers];

					QuizClientInteractive
							.printCLI("Now you can enter answers for this question. ");

					for (int i = 0; i < amountAnswers; i++) {
						Alternative altAnswer = new AlternativeImpl();

						altAnswer.id = (char) (97 + i);

						QuizClientInteractive
								.printCLI("Please enter the answer for answer "
										+ altAnswer.id);
						currentLine = in.readLine();
						altAnswer.sentence = currentLine;

						QuizClientInteractive
								.printCLI("Please enter true if this answer is correct, otherwise false");

						while (true) {
							currentLine = in.readLine();

							if (currentLine.equals("true")) {
								if (question.correctAlternatives != null) {
									char temp[] = question.correctAlternatives;
									question.correctAlternatives = new char[temp.length + 1];
									for (int j = 0; j < temp.length; j++) {
										question.correctAlternatives[j] = temp[j];
									}
									question.correctAlternatives[temp.length] = altAnswer.id;
								} else {
									question.correctAlternatives = new char[1];
									question.correctAlternatives[0] = altAnswer.id;
								}
								break;
							} else if (currentLine.equals("false")) {
								break;
							} else {
								QuizClientInteractive
										.printCLI("Could'nt read your input, please enter true if this answer is a correct, otherwise false");
								continue;
							}
						}

						question.alternatives[i] = altAnswer;

					}
					break;
				}

				try {
					quizImpl.newQuestion(question);
				} catch (QuizException e) {
					QuizClientInteractive.printCLI(e.reason);
				}
			} else if (currentLine.equals("3")) {
				int questionId;

				QuizClientInteractive
						.printCLI("Please enter valid a question ID");

				while (true) {
					try {
						currentLine = in.readLine();
						questionId = Integer.parseInt(currentLine);
					} catch (NumberFormatException e) {
						QuizClientInteractive
								.printCLI("Id not readable, please try again.");
						continue;
					}

					break;
				}

				QuizClientInteractive
						.printCLI("Are you sure that you want delete question with id "
								+ questionId + "? y or n");
				boolean sure = false;
				while (true) {
					currentLine = in.readLine();
					if (currentLine.equals("y"))
						sure = true;
					else if (currentLine.equals("n"))
						sure = false;
					else {
						QuizClientInteractive
								.printCLI("Can't read input, please try again");
						continue;
					}
					break;
				}

				if (sure) {
					try {
						quizImpl.removeQuestion(questionId);
					} catch (QuizException e) {
						QuizClientInteractive.printCLI(e.reason);
					}
				}

			} else if (currentLine.equals("4")) {

				Question random;

				try {
					random = quizImpl.getRandomQuestion();
				} catch (QuizException e) {
					QuizClientInteractive.printCLI(e.reason);
					continue;
				}

				System.out.println("Id " + random.id + ": " + random.sentence);
				for (int i = 0; i < random.alternatives.length; i++) {
					System.out.println(random.alternatives[i].id + ") "
							+ random.alternatives[i].sentence);
				}

			} else if (currentLine.equals("5")) {
				break;
			} else {
				QuizClientInteractive
						.printCLI(menu
								+ "Please try again, I could'nt unterstand your input...");
			}
		}

	}

}
