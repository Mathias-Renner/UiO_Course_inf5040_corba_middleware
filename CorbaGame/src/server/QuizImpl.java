package server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Quiz.CompleteQuestion;
import Quiz.Question;
import Quiz.QuizServerPOA;
import Quiz.QuizServerPackage.QuizException;
import Quiz.QuizServerPackage.alternativesIdsHolder;

/**
 * This class extends the Portable Object Adapter Class (POA) and handles remote invocations from clients.
 * Here is the logic of the server's tasks implemented corresponding to the methods defined in the POA interface.
 */
public class QuizImpl extends QuizServerPOA{

	private Map<Integer,CompleteQuestion> cQuestions = new HashMap<Integer,CompleteQuestion>();
	
	@Override
	public int newQuestion(CompleteQuestion question) throws QuizException {
	
		
		if(question == null)
			throw new QuizException("CompleteQuestion cannot be null");
		
		if(cQuestions.containsKey(question.id))
			throw new QuizException("ID of CompleteQuestion is already in use");
		
		if(question.sentence == null && question.sentence.equals(""))
			throw new QuizException("Question cannot be null or empty");

		if(question.alternatives == null && question.alternatives.length > 0)
			throw new QuizException("No Alternative available");
		
		if(question.correctAlternatives == null && question.correctAlternatives.length > 0 && question.correctAlternatives.length <= question.alternatives.length )
			throw new QuizException("Correct answers are not well structured");
		
		// check if there is an answer resp. alternatives
		boolean isAnswer = false;

		for(int i = 0; i < question.alternatives.length; i++)
		{
			for (int j = 0; j < question.correctAlternatives.length; j++) {
				
				if(question.correctAlternatives[j] == question.alternatives[i].id)
					isAnswer = true;
			}
		}
		
		if(isAnswer == false)
			throw new QuizException("No correct answer defined. At least one correct answer has to be available");
		
		// put the question to the map, the collection of questions
		this.cQuestions.put(question.id, question);
		
		return question.id;
	}

	@Override
	public Question getRandomQuestion() throws QuizException {
		
		if(cQuestions.isEmpty())
			throw new QuizException("No questions available");
		
		List<CompleteQuestion> questionList = new ArrayList<CompleteQuestion>(cQuestions.values());
		Collections.shuffle(questionList);
		return questionList.get(0);
	}

	@Override
	public boolean answerQuestion(int questionId, char[] answer, alternativesIdsHolder correct) throws QuizException {
		
		if(!cQuestions.containsKey(questionId))
			throw new QuizException("No question with this id available");
		
		CompleteQuestion cQuestion = this.cQuestions.get(questionId);
		correct.value = cQuestion.correctAlternatives;
		
		if(answer.length != cQuestion.correctAlternatives.length)
			return false;
		
		for (int i = 0; i < cQuestion.correctAlternatives.length; i++) {
			boolean isAvailable = false;
			for (int j = 0; j < answer.length; j++) {
				if(cQuestion.correctAlternatives[i] == answer[j])
				{
					isAvailable = true;
					break;
				}
			}
			
			if(isAvailable == false)
				return false;
		}
		
		return true;
	}

	@Override
	public int removeQuestion(int questionId) throws QuizException {
		
		if(cQuestions.containsKey(questionId))
			cQuestions.remove(questionId);
		else
			throw new QuizException("Question with Id" + questionId + " does not exist");
		
		return questionId;
	}

}
