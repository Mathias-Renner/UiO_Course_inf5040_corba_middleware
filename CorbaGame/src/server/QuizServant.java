package server;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import Quiz.QuizServer;
import Quiz.QuizServerHelper;


/**
 * This class is initializes the server:
 * 1) create and initialize the ORB
 * 2) get reference to rootpoa & activate the POAManager
 * 3) create servant and register it with the ORB
 * 4) get the root naming context
 * 5) bind the Object Reference in Naming
 * 6) waits for invocations from clients
 * 
 * Run this class with following configuration:
 * 
 * -ORBInitialPort port -ORBInitialHost host
 * 
 * port defines the port of the server (name service)
 * host defines the host of the server (name service)
 * 
 * If host is localhost then remove "-ORBIntitialHost host"
 * 
 */
public class QuizServant{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			// get reference to rootpoa & activate the POAManager
			POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootpoa.the_POAManager().activate();

			// create servant and register it with the ORB
			QuizImpl quizImpl = new QuizImpl();
			org.omg.CORBA.Object ref = rootpoa.servant_to_reference(quizImpl);
			QuizServer href = QuizServerHelper.narrow(ref);

			// get the root naming context
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			// Use NamingContextExt which is part of the Interoperable
			// Naming Service (INS) specification.
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			
			// bind the Object Reference in Naming
			String name = "Quiz";
			NameComponent path[] = ncRef.to_name( name );
			ncRef.rebind(path, href);

			System.out.println("QuizServer ready and waiting ...");

			// wait for invocations from clients
			orb.run();
			
		}
		
		catch (Exception e) {
			System.err.println("ERROR: " + e);
			e.printStackTrace(System.out);
		}
	}
}
