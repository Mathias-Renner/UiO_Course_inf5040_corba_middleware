package server;

import Quiz.CompleteQuestion;
import Quiz.Question;
import Quiz.QuizServerPOA;
import Quiz.QuizServerPackage.QuizException;
import Quiz.QuizServerPackage.alternativesIdsHolder;

/**
 * @author Matth�us Huber, Eva As, Ulrike Janke, Mathias Renner
 *
 */
public class QuizImpl extends QuizServerPOA {

	public int newQuestion(CompleteQuestion question) throws QuizException {
		// TODO Create new question on server
		// return: Get question ID
		return 0;
	}

	public Question getRandomQuestion() throws QuizException {
		// TODO Get random Question from server
		// return: show Question
		return null;
	}

	public boolean answerQuestion(int questionId, char[] answer,
			alternativesIdsHolder correct) throws QuizException {
		// TODO Send answer
		// return: result
		return false;
	}

	public int removeQuestion(int questionId) throws QuizException {
		// TODO remove Question from question pool
		// return: deleted question ID
		return 0;
	}

}
